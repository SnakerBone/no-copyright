package au.com.snaker.nocopyright;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;

import static au.com.snaker.nocopyright.NoCopyright.MOD_ID;

@Mod(MOD_ID)
public class NoCopyright {
    public static final String MOD_ID = "nocopyright";

    public NoCopyright() {
        MinecraftForge.EVENT_BUS.register(this);
    }
}