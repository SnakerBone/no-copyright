package au.com.snaker.nocopyright.mixin;

import net.minecraft.client.gui.screen.MainMenuScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArgs;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.invoke.arg.Args;

@Mixin(MainMenuScreen.class)
public class NoCopyrightMixin {
    @ModifyArgs(method = "render", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/screen/MainMenuScreen;drawString(Lcom/mojang/blaze3d/matrix/MatrixStack;Lnet/minecraft/client/gui/FontRenderer;Ljava/lang/String;III)V", opcode = 1))
    private void clearLine(Args args) {
        args.set(2, "");
    }

    @ModifyArgs(method = "render", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/screen/MainMenuScreen;fill(Lcom/mojang/blaze3d/matrix/MatrixStack;IIIII)V"))
    private void injected(Args args) {
        args.set(1, 0);
        args.set(2, 0);
        args.set(3, 0);
        args.set(4, 0);
    }

    @Shadow
    private int widthCopyright;

    @Inject(method = "mouseClicked(DDI)Z", at = @At("HEAD"))
    private void injected(CallbackInfoReturnable ci) {
        this.widthCopyright = 0;
    }

}