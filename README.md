# No Copyright Mod

Removes the annoying copyright notice from the main menu

# About Mod

This is a fork of [Copyright Remover](https://www.curseforge.com/minecraft/mc-mods/copyright-remover) made to work with forge

## Credits

All credits go to the original mod creator [Ask6155](https://gitlab.com/ask6155)